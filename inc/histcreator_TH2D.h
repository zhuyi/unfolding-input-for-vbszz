#ifndef HIST_CREATOR_TH2D_H
#define HIST_CREATOR_TH2D_H

#include <vector>

#include "TString.h"

#include "histcreator.h"

class HistCreatorTH2D : public HistCreator
{
public:

    HistCreatorTH2D(TString hist_name, TString path_to_file);
    HistCreatorTH2D(TString hist_name, TString var_x, TString var_y, TString tree_name, TString path_to_file,
                TString global_weight, TString nominal_weight,
                const std::vector<double> &binning, const std::vector<TString> &files);
    ~HistCreatorTH2D() {};

    HistCreatorTH2D() = delete;
    HistCreatorTH2D(const HistCreatorTH2D&) = delete;
    HistCreatorTH2D& operator=(const HistCreatorTH2D&) = delete;

    void SetVariableY(TString new_variable_y) {variable_y_ = new_variable_y;}

protected:
    virtual void InitHist() override;
    virtual void FillHist() override;

    TString variable_y_{};
};

#endif
