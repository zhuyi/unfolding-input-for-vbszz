#ifndef ENVELOP_H
#define ENVELOP_H

#include "TH1F.h"
#include "TDirectory.h"

class Envelop
{
public:
    Envelop(std::vector<TString> *variations, TString path);
    ~Envelop();

    void ConstructHists();
    void SetTruthVariable(const TString &truth_var) {truth_var_ = truth_var;}
    void SetTruthWeight(const TString &truth_weight) {truth_weight_ = truth_weight;}

    void GetEnvelopedTH1Fs(TH1F *hist_up, TH1F *hist_down, const std::vector<TH1F*> &hists);

    TH1F* GetHistUp() const;
    TH1F* GetHistDown() const;

    void WriteUp(TDirectory *dir = nullptr);
    void WriteDown(TDirectory *dir = nullptr);

private:
    struct Node
    {
        double content{0.};
        double err{0.};
    };

    std::vector<TH1F*> fidcorrnums_;
    std::vector<TH1F*> fidcorrdens_;
    std::vector<TH1F*> effcorrnums_;
    std::vector<TH1F*> effcorrdens_;
    std::vector<TH1F*> puritynums_;

    TH1F *nominal_{nullptr};
    TH1F *fidcorrnum_up_{nullptr};
    TH1F *fidcorrden_up_{nullptr};
    TH1F *effcorrnum_up_{nullptr};
    TH1F *effcorrden_up_{nullptr};
    TH1F *puritynum_up_{nullptr};
    TH1F *fidcorrnum_down_{nullptr};
    TH1F *fidcorrden_down_{nullptr};
    TH1F *effcorrnum_down_{nullptr};
    TH1F *effcorrden_down_{nullptr};
    TH1F *puritynum_down_{nullptr};

    TString truth_var_;
    TString truth_weight_;

    TString path_to_variations_;
    std::vector<TString> *variations_{nullptr};
};

#endif
