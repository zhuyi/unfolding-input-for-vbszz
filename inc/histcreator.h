#ifndef HIST_CREATOR_H
#define HIST_CREATOR_H

#include <vector>

#include "TH1.h"
#include "TCut.h"
#include "TString.h"

class HistCreator
{
public:

    HistCreator(TString hist_name, TString path_to_file);
    HistCreator(TString hist_name, TString variable_x, TString tree_name, TString path_to_file,
                TString global_weight, TString nominal_weight,
                const std::vector<double> &binning, const std::vector<TString> &files);
    ~HistCreator() {};

    TH1* GetHist() const;

    HistCreator() = delete;
    HistCreator(const HistCreator&) = delete;
    HistCreator& operator=(const HistCreator&) = delete;

    void ConstructHist();

    void SetVariableX(TString new_variable_x) {variable_x_ = new_variable_x;}
    void SetNominalWeight(TString new_weight) {nominal_weight_ = new_weight;}
    void SetVariationWeight(TString variation);
    void SetVariationValues(std::vector<double> variation_values);
    void SetFiducial(bool if_fiducial = true) {if_fiducial_ = if_fiducial;}
    void SetRecoAndFiducial(bool if_reco_fiducial = true) {if_reco_fiducial_ = if_reco_fiducial;}
    void SetInputs(const std::vector<TString> &inputs);
    void SetDirectory0();

    void Write(TString written_name = "");

protected:
    virtual void InitHist() {}
    virtual void FillHist() {}

    TCut GetFinalCut();

    TH1 *hist_{nullptr};

    TString hist_name_{};
    TString variable_x_{};
    TString tree_name_{};
    TString path_to_file_{};
    TString global_weight_{};
    TString nominal_weight_{};

    bool if_variation_weight_{false};
    TString variation_weight_{};
    bool if_variation_values_{false};
    std::vector<double> variation_values_;

    bool if_fiducial_{false};
    bool if_reco_fiducial_{false};

    std::vector<double> binning_;
    std::vector<TString> files_;
};

#endif
