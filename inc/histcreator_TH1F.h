#ifndef HIST_CREATOR_TH1F_H
#define HIST_CREATOR_TH1F_H

#include <vector>

#include "TString.h"

#include "histcreator.h"

class HistCreatorTH1F : public HistCreator
{
public:

    HistCreatorTH1F(TString hist_name, TString path_to_file);
    HistCreatorTH1F(TString hist_name, TString var, TString tree_name, TString path_to_file,
                TString global_weight, TString nominal_weight,
                const std::vector<double> &binning, const std::vector<TString> &files);
    ~HistCreatorTH1F() {};

    HistCreatorTH1F() = delete;
    HistCreatorTH1F(const HistCreatorTH1F&) = delete;
    HistCreatorTH1F& operator=(const HistCreatorTH1F&) = delete;

protected:
    virtual void InitHist() override;
    virtual void FillHist() override;
};

#endif
