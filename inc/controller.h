#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <string>
#include <map>

#include "TString.h"
#include "TCut.h"

using SystematicSetMap    = std::map<TString, std::map<TString, std::vector<TString>>>;
using SystematicValuesMap = std::map<TString, std::vector<double>>;
using SystematicNamesMap  = std::map<TString, std::vector<TString>>;

enum Mode{vUndefined, vSystematic, vUnfolding};

class Controller
{
public:
    static Controller* CreateInstance();

    bool ReadSetting(std::string setting);
    bool ReadSystematics(std::string systematic);
    bool ReadInput(std::string input);

    bool IfFakeData() const {return fake_data_;}

    TString GetOutName()  const {return out_name_;}
    TString GetTreeName() const {return tree_name_;}
    TString GetVariable() const {return variable_;}
    TString GetTruthVariable() const {return truth_variable_;}
    TString GetGlobalWeight()  const {return global_weight_;}
    TString GetNominalWeight() const {return nominal_weight_;}
    TString GetTruthWeight()   const {return truth_weight_;}
    TString GetPathToNominal()     const {return path_to_nominal_;}
    TString GetPathToIndependent() const {return path_to_independent_;}
    TString GetPathToFiducial()    const {return path_to_fiducial_;}

    std::vector<double>  GetBinning() const {return binning_;}
    std::vector<TString> GetSignalInputs()     const {return signal_inputs_;}
    std::vector<TString> GetBackgroundInputs() const {return background_inputs_.at(0);}
    std::vector<TString> GetDataInputs() const {return data_inputs_;}
    std::vector<TCut>    GetControlRegion()  const {return control_;}
    std::vector<TCut>    GetFiducialRegion() const {return fiducial_;}
    SystematicSetMap     GetNominalTreeSystematics() const {return nominal_tree_systematics_;}
    SystematicSetMap     GetIndependentSystematics() const {return independent_systematics_;}
    SystematicValuesMap  GetSignalSystematicValues() const {return signal_systematic_values_;}
    std::vector<TString> GetScaleVariations() const;

    std::vector<std::vector<TString>> GetBackgroundChannels() const {return background_inputs_;}
    std::vector<double>               GetScaleFactors()       const {return scale_factors_;}

    void Print();

private:
    Controller() {}
    ~Controller() {}

    Mode mode_;
    bool fake_data_{false};

    std::string systematic_;
    std::string setting_;
    std::string input_;

    TString out_name_;
    TString tree_name_;
    TString variable_;
    TString truth_variable_;
    TString global_weight_;
    TString nominal_weight_;
    TString truth_weight_;
    TString path_to_nominal_;
    TString path_to_independent_;
    TString path_to_fiducial_;

    std::vector<double> binning_;
    std::vector<TCut> control_;
    std::vector<TCut> fiducial_;

    SystematicSetMap nominal_tree_systematics_;
    SystematicSetMap independent_systematics_;

    SystematicValuesMap signal_systematic_values_;
    SystematicValuesMap background_systematic_values_;
    SystematicValuesMap overall_systematic_values_;

    SystematicNamesMap envelop_systematic_names_;

    std::vector<TString> signal_inputs_;
    std::vector<std::vector<TString>> background_inputs_;
    std::vector<double>  scale_factors_;
    std::vector<TString> data_inputs_;
};

extern Controller *vController;

#endif
