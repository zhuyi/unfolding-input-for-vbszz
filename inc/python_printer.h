#ifndef PYTHON_PRINTER_H
#define PYTHON_PRINTER_H

#include "TString.h"

#include <fstream>
#include <vector>

class PythonPrinter
{
public:
    PythonPrinter();
    ~PythonPrinter();

    void Chmod(TString mode = "744");

    void PrintHeader();
    void PrintDefMain();
    void PrintBinning(const std::vector<double> &binning);
    void PrintResponse(const TString &variable);
    void PrintBackground();
    void PrintGeneralVariation(const TString &variation);
    void PrintResponseVariation(const TString &variation);
    void PrintBackgroundVariation(const TString &variation);
    void PrintModel(const TString &method = "Bayesian3");
    void PrintRunning();
    void PrintEndl();

private:
    TString python_name_;
    std::ofstream python_file_;

    const TString indentation_{"    "};
};

#endif
