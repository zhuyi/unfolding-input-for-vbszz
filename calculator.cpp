#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include "TString.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TH1F.h"

#include "controller.h"
#include "histcreator_TH1F.h"
#include "envelop.h"

void Parser(int argc, char** argv, std::string &setting, std::string &systematic, std::string &input)
{
    for(int i = 1; i < argc; ++i)
    {
        std::string current_arg(argv[i]);

        if(current_arg == "-s" && (i + 1) < argc)
        {
            setting = argv[i + 1];
            continue;
        }

        if(current_arg == "-i" && (i + 1) < argc)
        {
            input = argv[i + 1];
            continue;
        }

        if(current_arg == "-v" && (i + 1) < argc)
        {
            systematic = argv[i + 1];
            continue;
        }
    }
}

int main(int argc, char** argv)
{
    std::string setting("setting.yaml");
    std::string systematic("systematic.yaml");
    std::string input("input.yaml");

    Parser(argc, argv, setting, systematic, input);

//..................................................//
//Greetings
    std::cout << std::endl
              << "Starting our fancy unfolding input creation ..." << std::endl
              << std::endl;

    std::cout << "INFO	Settings from   : " << setting << std::endl
              << "INFO	Inputs from     : " << input << std::endl
              << "INFO	Systematics from: " << systematic << std::endl
              << std::endl;

    Controller::CreateInstance();
    vController->ReadSetting(setting);
    vController->ReadSystematics(systematic);
    vController->ReadInput(input);
    vController->Print();

//..................................................//
//Manage settings
    TString output(vController->GetOutName());

    std::ofstream txt(output + ".txt");

    TFile *file = new TFile(output + ".root", "recreate");

    TDirectory *nominal_dir = file->mkdir("Nominal");
    nominal_dir->cd();

    HistCreatorTH1F nominal("nominal", vController->GetPathToNominal());
    nominal.ConstructHist();
    nominal.Write();

    file->cd();

    //double nominal_yield = dynamic_cast<TH1F*>(nominal.GetHist())->GetSumOfWeights();
    std::vector<double> nominal_yields;
    for(int i = 1; i <= nominal.GetHist()->GetNbinsX(); i++)
        nominal_yields.push_back(nominal.GetHist()->GetBinContent(i));

    auto nominal_systematics = vController->GetNominalTreeSystematics();
    for(const auto &set : nominal_systematics)
    {
        txt << set.first << std::endl;

        TDirectory *set_dir = file->mkdir(set.first);
        set_dir->cd();

        for(const auto &systematic : set.second)
        {
            TDirectory *systematic_dir = set_dir->mkdir(systematic.first);
            systematic_dir->cd();

            for(auto variation : systematic.second)
            {
                HistCreatorTH1F hist(variation, vController->GetPathToNominal());
                hist.SetVariationWeight(variation);
                hist.ConstructHist();
                hist.Write();

                //double variation_yield = dynamic_cast<TH1F*>(hist.GetHist())->GetSumOfWeights();
                //txt << variation << "	"
                //    << (variation_yield - nominal_yield)/nominal_yield << std::endl;
                txt << variation << "	";
                for(int i = 1; i <= hist.GetHist()->GetNbinsX(); i++)
                    txt << (hist.GetHist()->GetBinContent(i) - nominal_yields.at(i - 1))/nominal_yields.at(i - 1) << "\t";
                txt << std::endl;
            }

            set_dir->cd();
        }

        file->cd();

        txt << std::endl;
    }

    file->cd();
    auto independent_systematics = vController->GetIndependentSystematics();
    for(const auto &set : independent_systematics)
    {
        txt << set.first << std::endl;

        TDirectory *set_dir = file->mkdir(set.first, set.first, true);
        set_dir->cd();

        for(const auto &systematic : set.second)
        {
            TDirectory *systematic_dir = set_dir->mkdir(systematic.first);
            systematic_dir->cd();

            for(auto variation : systematic.second)
            {
                HistCreatorTH1F hist(variation, vController->GetPathToIndependent() + "/" + variation + "/");
                hist.ConstructHist();
                hist.Write();

                //double variation_yield = dynamic_cast<TH1F*>(hist.GetHist())->GetSumOfWeights();
                //txt << variation << "	"
                //    << (variation_yield - nominal_yield)/nominal_yield << std::endl;
                txt << variation << "	";
                for(int i = 1; i <= hist.GetHist()->GetNbinsX(); i++)
                    txt << (hist.GetHist()->GetBinContent(i) - nominal_yields.at(i - 1))/nominal_yields.at(i - 1) << "\t";
                txt << std::endl;
                //for(int i = 1; i <= hist.GetHist()->GetNbinsX(); i++)
                //    std::cout << (hist.GetHist()->GetBinContent(i) - nominal_yields.at(i - 1))/nominal_yields.at(i - 1) << "\t";
                //std::cout << std::endl;
            }

            set_dir->cd();
        }

        file->cd();

        txt << std::endl;
    }

    file->Close();

    txt.close();

    std::cout << std::endl
              << "INFO	End creating outputs ..." << std::endl
              << std::endl;
}
