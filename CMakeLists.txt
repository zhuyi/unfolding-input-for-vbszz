
#----------------------------------------------------------------------------
# Setup the project
#
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(VBSZZ_Unfolding_Ntuple_Creator)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#----------------------------------------------------------------------------
# Find ROOT
# FIND_PACKAGE(OpenGL)

find_package(ROOT)
include(${ROOT_USE_FILE})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/inc)

find_package(yaml-cpp REQUIRED)
link_libraries(yaml-cpp)

#----------------------------------------------------------------------------
# Locate sources and headers for this project
# NB: headers are included so they will show up in IDEs
#
file(GLOB sources ${PROJECT_SOURCE_DIR}/src/*.cpp ${PROJECT_SOURCE_DIR}/src/*/*.cpp)
file(GLOB headers ${PROJECT_SOURCE_DIR}/inc/*.h   ${PROJECT_SOURCE_DIR}/inc/*/*.h)

#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
#
add_executable(calculator calculator.cpp ${sources} ${headers})
target_link_libraries(calculator ${ROOT_LIBRARIES})

add_executable(creator creator.cpp ${sources} ${headers})
target_link_libraries(creator ${ROOT_LIBRARIES})

#----------------------------------------------------------------------------
# Install the executable to directory defined by CMAKE_INSTALL_PREFIX and
# copy all scripts to the directory.
# This is so that we can run the executable directly because it
# relies on these scripts being in the current working directory.
#
set(CONFIGS
    script/setting.yaml
    script/input.yaml
    script/systematic.yaml
   )

install(TARGETS calculator DESTINATION .)
install(TARGETS creator DESTINATION .)
# install(FILES ${CONFIGS} DESTINATION .)
