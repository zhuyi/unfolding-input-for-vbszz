#include "python_printer.h"

#include <cstdlib>
#include <iostream>

PythonPrinter::PythonPrinter() : python_name_("VBS_unfold.py"),
                                 python_file_(python_name_.Data())
{
}

PythonPrinter::~PythonPrinter()
{
    python_file_.close();
}

void PythonPrinter::Chmod(TString mode)
{
    TString command("chmod " + mode + " " + python_name_);
    std::system(command.Data());
}

void PythonPrinter::PrintHeader()
{
    python_file_ << "#! /usr/bin/env python\n" << std::endl
                 << "import os\nimport sys" << std::endl
                 << "sys.path.append(os.path.join(os.getcwd(), '../../python_interface'))" << std::endl
                 << "import vipunfolding\n" << std::endl;
}

void PythonPrinter::PrintDefMain()
{
    python_file_ << "def main():\n" << std::endl;
}

void PythonPrinter::PrintBinning(const std::vector<double> &binning)
{
    python_file_ << indentation_ << "binning = [";
    for(auto bin : binning) python_file_ << bin << ", ";
    python_file_ << "]\n" << std::endl;
}

void PythonPrinter::PrintResponse(const TString &variable)
{
    python_file_ << indentation_ << "model = vipunfolding.Model('" << variable << "', binning, 'response.root')\n" << std::endl;
}

void PythonPrinter::PrintBackground()
{
    python_file_ << indentation_ << "model.add_variation('nominal', 'nominal', 'background.root:nominal/reconstructed')\n" << std::endl;
}

void PythonPrinter::PrintGeneralVariation(const TString &variation)
{
    python_file_ << indentation_ << "model.add_variation('" << variation << "', "
                 << "'response.root:" << variation << "', 'background.root:" << variation << "/reconstructed')" << std::endl;
}

void PythonPrinter::PrintResponseVariation(const TString &variation)
{
    python_file_ << indentation_ << "model.add_variation('" << variation << "', spath='response.root:" << variation << "')" << std::endl;
}

void PythonPrinter::PrintBackgroundVariation(const TString &variation)
{
    python_file_ << indentation_ << "model.add_variation('" << variation << "', bpath='background.root:" << variation << "/reconstructed')" << std::endl;
}

void PythonPrinter::PrintModel(const TString &method)
{
    python_file_ << indentation_ << "unfolding_method = '" << method << "'" << std::endl
                 << indentation_ << "vipunfolding.write_config(model, 'data.root:nominal/reconstructed', unfolding_method, xmethod='BinByBin')" << std::endl
                 << indentation_ << "vipunfolding.unfold(model, 'data.root:nominal/reconstructed', unfolding_method, xmethod='BinByBin')\n" << std::endl;
}

void PythonPrinter::PrintRunning()
{
    python_file_ << "if __name__ == '__main__':" << std::endl
                 << indentation_ << "main()" << std::endl;
}

void PythonPrinter::PrintEndl()
{
    python_file_ << std::endl;
}
