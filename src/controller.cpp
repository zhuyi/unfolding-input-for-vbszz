#include <iostream>
#include <vector>

#include "controller.h"

#include "yaml-cpp/yaml.h"

Controller *vController = nullptr;

Controller* Controller::CreateInstance()
{
    if(vController == nullptr) vController = new Controller();
    return vController;
}

bool Controller::ReadSetting(std::string setting)
{
    setting_ = setting;
    YAML::Node file = YAML::LoadFile(setting_);

    binning_.clear();
    control_.clear();
    fiducial_.clear();

    signal_inputs_.clear();
    background_inputs_.clear();
    data_inputs_.clear();

    try
    {
        std::string mode_str = file["mode"].as<std::string>();
        if     (mode_str == "systematic") mode_ = vSystematic;
        else if(mode_str == "unfolding")  mode_ = vUnfolding;
        else                              mode_ = vUndefined;

        if(mode_ == vUndefined)
        {
            std::cout << "Warning	Unknown mode" << std::endl;
            return false;
        }

        out_name_ = file["output"].IsDefined() ? file["output"].as<std::string>() : "out";
        tree_name_ = file["tree"].IsDefined() ? file["tree"].as<std::string>() : "tree_PFLOW";
        variable_ = file["variable"].as<std::string>();
        binning_ = file["binning"].as<std::vector<double>>();
        global_weight_ = file["global_weight"].IsDefined() ? file["global_weight"].as<std::string>() : "weight";
        nominal_weight_ = file["nominal_weight"].IsDefined() ? file["nominal_weight"].as<std::string>() : "";

        for(auto node : file["control"])
        {
            TString cut_str = node.as<std::string>();
            TCut cut(cut_str);

            control_.push_back(cut);
        }

        if(mode_ == vUnfolding)
        {
            fake_data_ = file["fake"].IsDefined() ? true : false;

            truth_weight_ = file["truth_weight"].as<std::string>();
            truth_variable_ = file["truth_variable"].as<std::string>();

            for(auto node : file["fiducial"])
            {
                TString cut_str = node.as<std::string>();
                TCut cut(cut_str);

                fiducial_.push_back(cut);
            }
        }
    }
    catch(YAML::BadConversion &e)
    {
        std::cerr << "WARNING	In " << setting_ << ", " << e.msg << std::endl << std::endl;
        return false;
    }
    catch(YAML::InvalidNode &e)
    {
        std::cerr << "WARNING	In " << setting_ << ", " << e.msg << std::endl << std::endl;
        return false;
    }

    return true;
}

bool Controller::ReadSystematics(std::string systematic)
{
    systematic_ = systematic;
    YAML::Node file = YAML::LoadFile(systematic_);

    nominal_tree_systematics_.clear();
    independent_systematics_.clear();

    signal_systematic_values_.clear();
    background_systematic_values_.clear();
    overall_systematic_values_.clear();

    envelop_systematic_names_.clear();

    try
    {
        path_to_nominal_ = file["nominal_tree_path"].as<std::string>();
        path_to_fiducial_ = file["fiducial_tree_path"].IsDefined() ? file["fiducial_tree_path"].as<std::string>() : "";

        for(auto node : file["nominal_tree"])
        {
            for(auto it_node = node.begin(); it_node != node.end(); ++it_node)
            {
                std::map<TString, std::vector<TString>> systematic_set;
                std::string set_name = it_node->first.as<std::string>();

                for(auto variation_pair : it_node->second)
                {
                    TString systematic_name = variation_pair[0].as<std::string>();
                    TString systematic_up;
                    TString systematic_down;

                    int pair_size = variation_pair.size();
                    if(pair_size == 1)
                    {
                        systematic_up = "weight_" + systematic_name + "up";
                        systematic_down = "weight_" + systematic_name + "down";
                    }
                    else if(pair_size == 3)
                    {
                        systematic_up = variation_pair[1].as<std::string>();
                        systematic_down = variation_pair[2].as<std::string>();
                    }
                    else
                    {
                        std::cout << "WARNING	In " << systematic_ << " wrong systematic form at set "
                                  << set_name << " of a size " << pair_size << std::endl << std::endl;
                        return false;
                    }

                    systematic_set.insert(std::make_pair(systematic_name, std::vector<TString>({systematic_up, systematic_down})));
                }

                nominal_tree_systematics_.insert(std::make_pair(set_name, systematic_set));
            }
        }

        path_to_independent_ = file["independent_tree_path"].as<std::string>();

        for(auto node : file["independent_tree"])
        {
            for(auto it_node = node.begin(); it_node != node.end(); ++it_node)
            {
                std::map<TString, std::vector<TString>> systematic_set;
                std::string set_name = it_node->first.as<std::string>();

                for(auto variation_pair : it_node->second)
                {
                    TString systematic_name = variation_pair[0].as<std::string>();
                    TString systematic_up;
                    TString systematic_down;

                    int pair_size = variation_pair.size();
                    if(pair_size == 1)
                    {
                        systematic_up = systematic_name + "up";
                        systematic_down = systematic_name + "down";
                    }
                    else if(pair_size == 3)
                    {
                        systematic_up = variation_pair[1].as<std::string>();
                        systematic_down = variation_pair[2].as<std::string>();
                    }
                    else
                    {
                        std::cout << "WARNING   In " << systematic_ << " wrong systematic form at set "
                                  << set_name << " of a size " << pair_size << std::endl << std::endl;
                        return false;
                    }

                    systematic_set.insert(std::make_pair(systematic_name, std::vector<TString>({systematic_up, systematic_down})));
                }

                independent_systematics_.insert(std::make_pair(set_name, systematic_set));
            }
        }

        if(file["theoretical"]["scale"].IsDefined())
        {
            std::vector<TString> scales;

            for(auto node : file["theoretical"]["scale"]) scales.push_back(node.as<std::string>());
            envelop_systematic_names_.insert({"scale", scales});
        }

        if(file["values"]["signal"].IsDefined())
        {
            for(auto node : file["values"]["signal"])
            {
                TString name = node[0].as<std::string>();
    
                int node_size = node.size();
                std::vector<double> variation_vec;
                for(int i = 1; i < node_size; ++i) variation_vec.push_back(node[i].as<double>());

                signal_systematic_values_.insert(std::make_pair(name, variation_vec));
            }
        }
        if(file["values"]["background"].IsDefined())
        {
            for(auto node : file["values"]["background"])
            {
                TString name = node[0].as<std::string>();
 
                int node_size = node.size();
                std::vector<double> variation_vec;
                for(int i = 1; i < node_size; ++i) variation_vec.push_back(node[i].as<double>());

                background_systematic_values_.insert(std::make_pair(name, variation_vec));
            }
        }
        if(file["values"]["overall"].IsDefined())
        { 
            for(auto node : file["values"]["overall"])
            {
                TString name = node[0].as<std::string>();
 
                int node_size = node.size();
                std::vector<double> variation_vec;
                for(int i = 1; i < node_size; ++i) variation_vec.push_back(node[i].as<double>());

                overall_systematic_values_.insert(std::make_pair(name, variation_vec));
            }
        }
    }
    catch(YAML::BadConversion &e)
    {
        std::cerr << "WARNING	In " << systematic_ << ", " << e.msg << std::endl << std::endl;
        return false;
    }
    catch(YAML::InvalidNode &e)
    {
        std::cerr << "WARNING	In " << systematic_ << ", " << e.msg << std::endl << std::endl;
        return false;
    }

    return true;
}

bool Controller::ReadInput(std::string input)
{
    input_ = input;
    YAML::Node file = YAML::LoadFile(input_);

    signal_inputs_.clear(); std::vector<TString>().swap(signal_inputs_);
    background_inputs_.clear();
    scale_factors_.clear(); std::vector<double>().swap(scale_factors_);

    try
    {
        for(auto node : file["signal"])
        {
            std::vector<std::string> inputs_str = node.as<std::vector<std::string>>();

            for(const auto &input : inputs_str)
                signal_inputs_.push_back(input);
        }

        for(auto node : file["background"])
        {
            std::vector<std::string> inputs_str = node.as<std::vector<std::string>>();
            std::vector<TString> inputs;

            for(const auto &input : inputs_str)
                inputs.push_back(input);
            background_inputs_.push_back(inputs);
        }

        if(file["fake"].IsDefined() && file["data"].IsDefined())
        {
            for(auto node : file["data"])
            {
                auto inputs_str = node.as<std::vector<std::string>>();

                for(const auto &input : inputs_str)
                    data_inputs_.push_back(input);
            }
        }

        if(file["scale_factor"].IsDefined())
            scale_factors_ = file["scale_factor"].as<std::vector<double>>();
    }
    catch(YAML::BadConversion &e)
    {
        std::cerr << "WARNING	In " << input_ << ", " << e.msg << std::endl << std::endl;
        return false;
    }
    catch(YAML::InvalidNode &e)
    {
        std::cerr << "WARNING	In " << input_ << ", " << e.msg << std::endl << std::endl;
        return false;
    }

    return true;
}

void Controller::Print()
{
    std::cout << "INFO	Output file name: " << out_name_ << std::endl << std::endl;

    if     (mode_ == vSystematic)
    {
        std::cout << "INFO	Variable to calculate systematics:" << std::endl
                  << "    	  - " << variable_ << std::endl;
    }
    else if(mode_ == vUnfolding)
    {
        std::cout << "INFO	Variable to unfold:" << std::endl
                  << "    	  - " << variable_ << std::endl;

        std::cout << "INFO	Truth variable to unfold:" << std::endl
                  << "    	  - " << truth_variable_ << std::endl;
    }

    std::cout << "INFO	Binning of the variable:" << std::endl
              << "    	  - ";
    for(auto bin : binning_) std::cout << bin << "	";
    std::cout << std::endl << std::endl;

    std::cout << "INFO	Control region:" << std::endl;
    for(auto cut : control_)
        std::cout << "    	  - " << cut << std::endl;
    std::cout << std::endl;

    if(mode_ == vUnfolding)
    {
        std::cout << "INFO	Fiducial phase space:" << std::endl;
        for(auto cut : fiducial_)
            std::cout << "    	  - " << cut << std::endl;
        std::cout << std::endl;
    }

    std::cout << "INFO	Systematics in nominal trees to calculate:" << std::endl;
    for(auto systematic_set : nominal_tree_systematics_)
    {
        std::cout << "    	  - Set: " << systematic_set.first << std::endl;
        for(auto systematic : systematic_set.second)
        {
            std::cout << "    	         - " << systematic.first << ": " << std::endl;
            for(auto variation : systematic.second)
            std::cout <<  "    	             " << variation << std::endl;
        }
    }
    if(envelop_systematic_names_.count("scale"))
    {
        std::cout << "    	  - Set: Scale" << std::endl;       
        for(auto variation : envelop_systematic_names_.at("scale"))
            std::cout << "    	             " << variation << std::endl;
    }
    std::cout << std::endl;

    std::cout << "INFO	Systematics in independent trees to calculate:" << std::endl;
    for(auto systematic_set : independent_systematics_)
    {
        std::cout << "    	  - Set: " << systematic_set.first << std::endl;
        for(auto systematic : systematic_set.second)
        {
            std::cout << "    	         - " << systematic.first << ": " << std::endl;
            for(auto variation : systematic.second)
            std::cout <<  "    	             " << variation << std::endl;
        }
    }
    std::cout << std::endl;

    if(signal_systematic_values_.size() || background_systematic_values_.size() || overall_systematic_values_.size())
    {
        std::cout << "INFO	Systematics values to calculate:" << std::endl;

        if(signal_systematic_values_.size())
        for(auto systematic_set : signal_systematic_values_)
        {
            std::cout << "    	  - Set: " << systematic_set.first;
            for(auto value : systematic_set.second) std::cout << "	" << value;
            std::cout << std::endl;
        }

        std::cout << std::endl;
    }

    if     (mode_ == vSystematic)
        std::cout << "INFO	Inputs:" << std::endl;
    else if(mode_ == vUnfolding)
        std::cout << "INFO	Signal inputs:" << std::endl;
    for(const auto &input : signal_inputs_)
        std::cout << "    	  - " << input << std::endl;

    if(mode_ == vUnfolding)
    {
        std::cout << "INFO	Background inputs:" << std::endl;
        for(const auto &inputs : background_inputs_)
        {
            for(const auto &input : inputs) std::cout << "    	  - " << input << std::endl;
            std::cout << "\t" << std::endl;
        }
    }

    //std::cout << std::endl;
}

std::vector<TString> Controller::GetScaleVariations() const
{
    if(envelop_systematic_names_.count("scale"))
        return envelop_systematic_names_.at("scale");

    return std::vector<TString>();
}
