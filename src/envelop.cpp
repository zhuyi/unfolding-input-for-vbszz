#include <iostream>

#include "envelop.h"
#include "histcreator_TH1F.h"

Envelop::Envelop(std::vector<TString> *variations, TString path) : path_to_variations_(path)
{
    variations_ = variations;
}

Envelop::~Envelop()
{
    variations_ = nullptr;

    for(int i = 0; i < fidcorrnums_.size(); i++)
    { delete fidcorrnums_.at(i); fidcorrnums_.at(i) = nullptr;}
    fidcorrnums_.clear();

    for(int i = 0; i < fidcorrdens_.size(); i++)
    { delete fidcorrdens_.at(i); fidcorrdens_.at(i) = nullptr;}
    fidcorrdens_.clear();

    for(int i = 0; i < effcorrnums_.size(); i++)
    { delete effcorrnums_.at(i); effcorrnums_.at(i) = nullptr;}
    effcorrnums_.clear();

    for(int i = 0; i < effcorrdens_.size(); i++)
    { delete effcorrdens_.at(i); effcorrdens_.at(i) = nullptr;}
    effcorrdens_.clear();

    for(int i = 0; i < puritynums_.size(); i++)
    { delete puritynums_.at(i); puritynums_.at(i) = nullptr;}
    puritynums_.clear();
}

void Envelop::ConstructHists()
{
    HistCreatorTH1F nominal_hist("scale_nominal", path_to_variations_);
    nominal_hist.ConstructHist();
    nominal_ = dynamic_cast<TH1F*>(nominal_hist.GetHist());

    for(const auto &variation : *variations_)
    {
        HistCreatorTH1F fidcorrnum("fidcorrnum_" + variation, path_to_variations_);
        fidcorrnum.SetRecoAndFiducial();
        fidcorrnum.SetVariationWeight("weight/weight_var_th_MUR1_MUF1_PDF261000*" + variation);
        fidcorrnum.ConstructHist();
        fidcorrnum.SetDirectory0();
        fidcorrnums_.push_back(dynamic_cast<TH1F*>(fidcorrnum.GetHist()));

        HistCreatorTH1F fidcorrden("fidcorrden_" + variation, path_to_variations_);
        fidcorrden.SetVariationWeight("weight/weight_var_th_MUR1_MUF1_PDF261000*" + variation);
        fidcorrden.ConstructHist();
        fidcorrden.SetDirectory0();
        fidcorrdens_.push_back(dynamic_cast<TH1F*>(fidcorrden.GetHist()));

        HistCreatorTH1F effcorrnum("effcorrnum_" + variation, path_to_variations_);
        effcorrnum.SetVariableX(truth_var_);
        effcorrnum.SetRecoAndFiducial();
        effcorrnum.SetVariationWeight("weight/weight_var_th_MUR1_MUF1_PDF261000*" + variation);
        effcorrnum.ConstructHist();
        effcorrnum.SetDirectory0();
        effcorrnums_.push_back(dynamic_cast<TH1F*>(effcorrnum.GetHist()));

        HistCreatorTH1F effcorrden("effcorrden_" + variation, path_to_variations_);
        effcorrden.SetVariableX(truth_var_);
        effcorrden.SetNominalWeight(variation);
        effcorrden.SetFiducial();
        effcorrden.ConstructHist();
        effcorrden.SetDirectory0();
        effcorrdens_.push_back(dynamic_cast<TH1F*>(effcorrden.GetHist()));

        HistCreatorTH1F puritynum("puritynum_" + variation, path_to_variations_);
        puritynum.SetVariableX(truth_var_);
        puritynum.SetRecoAndFiducial();
        puritynum.SetNominalWeight(variation);
        puritynum.ConstructHist();
        puritynum.SetDirectory0();
        puritynums_.push_back(dynamic_cast<TH1F*>(puritynum.GetHist()));
    }

    fidcorrnum_up_ = dynamic_cast<TH1F*>(nominal_->Clone());
    fidcorrnum_down_ = dynamic_cast<TH1F*>(nominal_->Clone());
    this->GetEnvelopedTH1Fs(fidcorrnum_up_, fidcorrnum_down_, fidcorrnums_);

    fidcorrden_up_ = dynamic_cast<TH1F*>(nominal_->Clone());
    fidcorrden_down_ = dynamic_cast<TH1F*>(nominal_->Clone());
    this->GetEnvelopedTH1Fs(fidcorrden_up_, fidcorrden_down_, fidcorrdens_);

    effcorrnum_up_ = dynamic_cast<TH1F*>(nominal_->Clone());
    effcorrnum_down_ = dynamic_cast<TH1F*>(nominal_->Clone());
    this->GetEnvelopedTH1Fs(effcorrnum_up_, effcorrnum_down_, effcorrnums_);

    effcorrden_up_ = dynamic_cast<TH1F*>(nominal_->Clone());
    effcorrden_down_ = dynamic_cast<TH1F*>(nominal_->Clone());
    this->GetEnvelopedTH1Fs(effcorrden_up_, effcorrden_down_, effcorrdens_);

    puritynum_up_ = dynamic_cast<TH1F*>(nominal_->Clone());
    puritynum_down_ = dynamic_cast<TH1F*>(nominal_->Clone());
    this->GetEnvelopedTH1Fs(puritynum_up_, puritynum_down_, puritynums_);
}

void Envelop::GetEnvelopedTH1Fs(TH1F *hist_up, TH1F *hist_down, const std::vector<TH1F*> &hists)
{
    std::vector<double> variations_up;
    std::vector<double> variations_down;
    std::vector<double> errors_up;
    std::vector<double> errors_down;
    for(int i = 1; i <= nominal_->GetNbinsX(); i++)
    {
        std::vector<Node> variations_yield;
        for(const auto &hist : hists)
            variations_yield.push_back(Node{hist->GetBinContent(i), hist->GetBinError(i)});

        std::sort(variations_yield.begin(),
                  variations_yield.end(),
                  [](const Node &node1, const Node &node2){return node1.content < node2.content;});

        variations_up.push_back(variations_yield.back().content);
        variations_down.push_back(variations_yield.front().content);
        errors_up.push_back(variations_yield.back().err);
        errors_down.push_back(variations_yield.front().err);
    }

    for(int i = 1; i <= hist_up->GetNbinsX(); i++)
    {
        //std::cout << variations_up.at(i - 1) << "\t";
        hist_up->SetBinContent(i, variations_up.at(i - 1));
        hist_up->SetBinError  (i, errors_up.at(i - 1));
    }
    //std::cout << std::endl;

    for(int i = 1; i <= hist_down->GetNbinsX(); i++)
    {
        hist_down->SetBinContent(i, variations_down.at(i - 1));
        hist_down->SetBinError  (i, errors_down.at(i - 1));
    }
}

TH1F* Envelop::GetHistUp() const
{
    return fidcorrnum_up_;
}

TH1F* Envelop::GetHistDown() const
{
    return fidcorrnum_down_;
}

void Envelop::WriteUp(TDirectory *dir)
{
    if(fidcorrden_up_)
    {
        fidcorrnum_up_->SetNameTitle("fidcorrnum", "fidcorrnum");
        fidcorrden_up_->SetNameTitle("fidcorrden", "fidcorrden");
        effcorrnum_up_->SetNameTitle("effcorrnum", "effcorrnum");
        effcorrden_up_->SetNameTitle("effcorrden", "effcorrden");
        puritynum_up_->SetNameTitle("puritynum", "puritynum");

        std::cout << "INFO	Hist " << fidcorrnum_up_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << fidcorrden_up_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << effcorrnum_up_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << effcorrden_up_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << puritynum_up_->GetName() << " stored" << std::endl;

        if(dir) dir->cd();
        fidcorrnum_up_->Write();
        fidcorrden_up_->Write();
        effcorrnum_up_->Write();
        effcorrden_up_->Write();
        puritynum_up_->Write();
    }
    else std::cout << "WARNING	No hist to store" << std::endl;
}

void Envelop::WriteDown(TDirectory *dir)
{
    if(fidcorrden_down_)
    {
        fidcorrnum_down_->SetNameTitle("fidcorrnum", "fidcorrnum");
        fidcorrden_down_->SetNameTitle("fidcorrden", "fidcorrden");
        effcorrnum_down_->SetNameTitle("effcorrnum", "effcorrnum");
        effcorrden_down_->SetNameTitle("effcorrden", "effcorrden");
        puritynum_down_->SetNameTitle("puritynum", "puritynum");

        std::cout << "INFO	Hist " << fidcorrnum_down_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << fidcorrden_down_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << effcorrnum_down_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << effcorrden_down_->GetName() << " stored" << std::endl;
        std::cout << "INFO	Hist " << puritynum_down_->GetName() << " stored" << std::endl;

        if(dir) dir->cd();
        fidcorrnum_down_->Write();
        fidcorrden_down_->Write();
        effcorrnum_down_->Write();
        effcorrden_down_->Write();
        puritynum_down_->Write();
    }
    else std::cout << "WARNING	No hist to store" << std::endl;
}
