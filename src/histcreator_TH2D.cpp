#include "histcreator_TH2D.h"

#include <iostream>

#include "TSystem.h"
#include "TDirectory.h"
#include "TH2D.h"
#include "TChain.h"
#include "TLeaf.h"
#include "TROOT.h"
#include "TCut.h"

#include "controller.h"

HistCreatorTH2D::HistCreatorTH2D(TString hist_name, TString path_to_file)
                                : HistCreator(hist_name, path_to_file),
                                  variable_y_("")
{
}

HistCreatorTH2D::HistCreatorTH2D(TString hist_name, TString variable_x, TString variable_y, TString tree_name, TString path_to_file,
                         TString global_weight, TString nominal_weight,
                         const std::vector<double> &binning, const std::vector<TString> &files)
                         : HistCreator(hist_name, variable_x, tree_name, path_to_file,
                                       global_weight, nominal_weight,
                                       binning, files),
                           variable_y_(variable_y)
{
}

void HistCreatorTH2D::InitHist()
{
    double xbin[binning_.size()];
    for(int i = 0; i < binning_.size(); ++i) xbin[i] = binning_.at(i);

    int nx = sizeof(xbin)/sizeof(xbin[0]) - 1;
    hist_ = new TH2D(hist_name_, hist_name_, nx, xbin, nx, xbin);
}

void HistCreatorTH2D::FillHist()
{
    if(hist_)
    {
        TCut final_cuts(this->GetFinalCut());

        TChain *tree = new TChain(tree_name_);
        for(auto file : files_) tree->Add(path_to_file_ + "/" + file);

        if(!gROOT->FindObject("c1"))
        std::cout << "INFO	";

        tree->Draw(variable_y_ + ":" + variable_x_ + ">>" + hist_->GetName(),
                   final_cuts,
                   "hist");

        if(if_variation_values_)
        {
            if((variation_values_.size() != hist_->GetNbinsX() && variation_values_.size() != 1) ||
               variation_values_.size() == 0)
            std::cout << "Warning	Different bin numbers in variation vector" << std::endl;

            if(variation_values_.size() == 1) hist_->Scale(variation_values_.at(0));
            else
            {
                for(int i = 0; i < variation_values_.size(); ++i)
                {
                    for(int j = 0; j < hist_->GetNbinsY(); ++j)
                    {
                        hist_->SetBinContent(i + 1, j + 1,
                                             hist_->GetBinContent(i + 1, j + 1)*(1 + variation_values_.at(i)));
                    }
                }
            }
        }

        hist_->GetXaxis()->SetTitle(variable_x_);
        hist_->GetYaxis()->SetTitle(variable_y_);

        delete tree; tree = nullptr;
    }
    else
        std::cout << "WARNING	No hist to fill" << std::endl;
}
