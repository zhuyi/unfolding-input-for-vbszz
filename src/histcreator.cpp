#include "histcreator.h"

#include <iostream>

#include "controller.h"

HistCreator::HistCreator(TString hist_name, TString path_to_file)
                         : hist_(nullptr),

                           hist_name_(hist_name),
                           variable_x_(vController->GetVariable()),
                           tree_name_(vController->GetTreeName()),
                           path_to_file_(path_to_file),
                           global_weight_(vController->GetGlobalWeight()),
                           nominal_weight_(vController->GetNominalWeight()),

                           if_variation_weight_(false),
                           variation_weight_(""),
                           if_variation_values_(false),

                           if_fiducial_(false),
                           if_reco_fiducial_(false),

                           binning_(vController->GetBinning()),
                           files_(vController->GetSignalInputs())
{
}

HistCreator::HistCreator(TString hist_name, TString variable, TString tree_name, TString path_to_file,
                         TString global_weight, TString nominal_weight,
                         const std::vector<double> &binning, const std::vector<TString> &files)
                         : hist_(nullptr),

                           hist_name_(hist_name),
                           variable_x_(variable),
                           tree_name_(tree_name),
                           path_to_file_(path_to_file),
                           global_weight_(global_weight),
                           nominal_weight_(nominal_weight),

                           if_variation_weight_(false),
                           variation_weight_(""),
                           if_variation_values_(false),

                           if_fiducial_(false),
                           if_reco_fiducial_(false),

                           binning_(binning),
                           files_(files)
{
}

TH1* HistCreator::GetHist() const
{
    if(!hist_) std::cout << "WARNING	Hist doesn't exist, null pointor catched" << std::endl;

    return hist_;
}

void HistCreator::ConstructHist()
{
    this->InitHist();
    this->FillHist();
}

void HistCreator::SetVariationWeight(TString variation)
{
    if_variation_weight_ = true;
    variation_weight_ = variation;
}

void HistCreator::SetVariationValues(std::vector<double> variation_values)
{
    if_variation_values_ = true;
    variation_values_.clear();
    variation_values_.assign(variation_values.begin(), variation_values.end());
}

void HistCreator::SetInputs(const std::vector<TString> &files)
{
    files_.clear();
    files_.assign(files.begin(), files.end());
}

void HistCreator::SetDirectory0()
{
    if(hist_) hist_->SetDirectory(nullptr);
}

void HistCreator::Write(TString written_name)
{
    if(hist_)
    {
        std::cout << "INFO	Hist " << hist_->GetName() << " stored" << std::endl;

        if(written_name == "") hist_->Write();
        else                   hist_->Write(written_name);
    }
    else
        std::cout << "WARNING	No hist to store" << std::endl;
}

TCut HistCreator::GetFinalCut()
{
    TCut region_cuts;
    if     (if_fiducial_ && !if_reco_fiducial_)
        for(auto cut : vController->GetFiducialRegion()) region_cuts = region_cuts && cut;
    else if(if_reco_fiducial_)
    {
        for(auto cut : vController->GetFiducialRegion()) region_cuts = region_cuts && cut;
        for(auto cut : vController->GetControlRegion())  region_cuts = region_cuts && cut;
    }
    else
        for(auto cut : vController->GetControlRegion()) region_cuts = region_cuts && cut;

    TCut final_cuts;
    if(if_variation_weight_) final_cuts = region_cuts*variation_weight_*global_weight_;
    else                     final_cuts = region_cuts*nominal_weight_*global_weight_;

    return final_cuts;
}
