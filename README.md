# Unfolding ntuple creator for VBSZZ

[[_TOC_]]

## How to setup & run

### How to setup
in your fancy path:
```shell script
mkdir source build run
git clone ssh://git@gitlab.cern.ch:7999/zhuyi/unfolding-input-for-vbszz.git source
cd build
source ../source/setup.sh
cmake -DCMAKE_INSTALL_PREFIX=../run ../source
make -j100
make install
```

### How to run
**Notion: paths in input.xml are those on SJTU INPAC cluster, substitute them with where you work**
```shell script
cd ../run
./creator
```

## Config files
We use yaml files as configs
### setting.yaml
Set how hists should be created

variable: choose the variable to be drawn
```yaml
variable: "met_tst"
```
binning: choose the binning of the hists
```yaml
binning: [90., 120., 150., 200., 250., 320., 400., 1000.]
```
fiducial: fiducial phase space/truth level selections
```yaml
fiducial: ["truth_leading_pT_lepton>30&&truth_subleading_pT_lepton>20"]
```
signal: systematics affect signals only

background: systematics affect backgrounds only

overall: systematics affect both signals and backgrounds
```yaml
systematics:
    signal:
        - ["pdf",  0.0156146,  -0.0156146]
    background:
        - ["test", 0.02, -0.02]
    overall:
```

### input.yaml
Set which samples to be drawn

signal: input signal samples
```yaml
signal:
    - ["*364285*.root"] # EWKZZ llvvjj
```
background: input background samples
```yaml
background:
    - ["*345723*.root", "*345666*.root"] # QCDZZ llvvjj
```
