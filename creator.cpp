#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include "TString.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TH1F.h"

#include "controller.h"
#include "histcreator_TH1F.h"
#include "histcreator_TH2D.h"
#include "envelop.h"
#include "python_printer.h"

void Parser(int argc, char** argv, std::string &setting, std::string &systematic, std::string &input)
{
    for(int i = 1; i < argc; ++i)
    {
        std::string current_arg(argv[i]);

        if(current_arg == "-s" && (i + 1) < argc)
        {
            setting = argv[i + 1];
            continue;
        }

        if(current_arg == "-i" && (i + 1) < argc)
        {
            input = argv[i + 1];
            continue;
        }

        if(current_arg == "-v" && (i + 1) < argc)
        {
            systematic = argv[i + 1];
            continue;
        }
    }
}

void PlotHistSet(TString path, TString truth_weight, TString truth_variable,
                 TString variation_weight = "", const std::vector<double> *variation_vec = nullptr)
{
    HistCreatorTH1F fidcorrnum("fidcorrnum", path);
    fidcorrnum.SetRecoAndFiducial();
    if(variation_weight != "") fidcorrnum.SetVariationWeight(variation_weight);
    if(variation_vec)          fidcorrnum.SetVariationValues(*variation_vec);
    fidcorrnum.ConstructHist();
    fidcorrnum.Write();

    HistCreatorTH1F fidcorrden("fidcorrden", path);
    if(variation_weight != "") fidcorrden.SetVariationWeight(variation_weight);
    if(variation_vec)          fidcorrden.SetVariationValues(*variation_vec);
    fidcorrden.ConstructHist();
    fidcorrden.Write();

    HistCreatorTH1F effcorrnum("effcorrnum", path);
    effcorrnum.SetVariableX(truth_variable);
    effcorrnum.SetRecoAndFiducial();
    if(variation_weight != "") effcorrnum.SetVariationWeight(variation_weight);
    if(variation_vec)          effcorrnum.SetVariationValues(*variation_vec);
    effcorrnum.ConstructHist();
    effcorrnum.Write();

    HistCreatorTH1F effcorrden("effcorrden", path);
    effcorrden.SetVariableX(truth_variable);
    effcorrden.SetNominalWeight(truth_weight);
    effcorrden.SetFiducial();
    //if(variation_weight != "") effcorrden.SetVariationWeight(variation_weight);
    //if(variation_vec)          effcorrden.SetVariationValues(*variation_vec);
    effcorrden.ConstructHist();
    effcorrden.Write();

    HistCreatorTH1F puritynum("puritynum", path);
    puritynum.SetVariableX(truth_variable);
    puritynum.SetRecoAndFiducial();
    puritynum.SetNominalWeight(truth_weight);
    //if(variation_weight != "") puritynum.SetVariationWeight(variation_weight);
    //if(variation_vec)          puritynum.SetVariationValues(*variation_vec);
    puritynum.ConstructHist();
    puritynum.Write();

    HistCreatorTH2D migration_matrix("migration_matrix", path);
    migration_matrix.SetVariableY(truth_variable);
    migration_matrix.SetRecoAndFiducial();
    if(variation_weight != "") migration_matrix.SetVariationWeight(variation_weight);
    if(variation_vec)          migration_matrix.SetVariationValues(*variation_vec);
    migration_matrix.ConstructHist();
    migration_matrix.Write();
}

int main(int argc, char** argv)
{
    TH1::SetDefaultSumw2();

    std::string setting("setting.yaml");
    std::string systematic("systematic.yaml");
    std::string input("input.yaml");

    Parser(argc, argv, setting, systematic, input);

//..................................................//
//Greetings
    std::cout << std::endl
              << "Starting our fancy unfolding input creation ..." << std::endl
              << std::endl;

    std::cout << "INFO	Settings from   : " << setting << std::endl
              << "INFO	Inputs from     : " << input << std::endl
              << "INFO	Systematics from: " << systematic << std::endl
              << std::endl;

    Controller::CreateInstance();
    vController->ReadSetting(setting);
    vController->ReadSystematics(systematic);
    vController->ReadInput(input);
    vController->Print();

    PythonPrinter printer;
    printer.PrintHeader();
    printer.PrintDefMain();
    printer.PrintBinning(vController->GetBinning());
    printer.PrintResponse(vController->GetVariable());

//..................................................//
//Manage settings
    TString path_to_nominal = vController->GetPathToNominal();
    TString path_to_fiducial = vController->GetPathToFiducial();
    TString path_to_independent = vController->GetPathToIndependent();
    TString truth_variable = vController->GetTruthVariable();
    TString truth_weight = vController->GetTruthWeight();

//..................................................//
//Signal inputs
    TFile *file = new TFile("response.root", "recreate");
    std::cout << "INFO	In file " << file->GetName() << ":" << std::endl;

    TString nominal_name("nominal");
    std::cout << "INFO	- Folder " << nominal_name << " created" << std::endl;

    TDirectory *nominal_dir = file->mkdir(nominal_name);
    nominal_dir->cd();

    auto signal_inputs = vController->GetSignalInputs();
    PlotHistSet(path_to_fiducial, truth_weight, truth_variable);

    file->cd();

    auto nominal_systematics = vController->GetNominalTreeSystematics();
    for(const auto &set : nominal_systematics)
    {
        for(const auto &systematic : set.second)
        {
            for(const auto &variation : systematic.second)
            {
                std::cout << "INFO	- Folder " << variation << " created" << std::endl;

                TDirectory *variation_dir = file->mkdir(variation);
                variation_dir->cd();

                PlotHistSet(path_to_fiducial, truth_weight, truth_variable, variation);
                //printer.PrintResponseVariation(variation);

                file->cd();
            }
        }
    }

    auto independent_systematics = vController->GetIndependentSystematics();
    for(const auto &set : independent_systematics)
    {
        for(const auto &systematic : set.second)
        {
            for(const auto &variation : systematic.second)
            {
                std::cout << "INFO	- Folder " << variation << " created" << std::endl;

                TDirectory *variation_dir = file->mkdir(variation);
                variation_dir->cd();

                PlotHistSet(path_to_independent + "/" + variation, truth_weight, truth_variable);
                //printer.PrintResponseVariation(variation);

                file->cd();
            }
        }
    }

    auto signal_systematic_values = vController->GetSignalSystematicValues();
    for(const auto &set : signal_systematic_values)
    {
        std::cout << "INFO	- Folder " << set.first << " created" << std::endl;

        TDirectory *variation_dir = file->mkdir(set.first);
        variation_dir->cd();

        PlotHistSet(path_to_fiducial, truth_weight, truth_variable, "", &set.second);
        printer.PrintResponseVariation(set.first);

        file->cd();
    }

/*
    auto scale_variations = vController->GetScaleVariations();
    if(scale_variations.size())
    {
        //Envelop scale(&scale_variations, path_to_fiducial);
        //scale.SetTruthVariable(truth_variable);
        //scale.ConstructHists();

        //std::cout << "INFO	- Folder scale__1up created" << std::endl;
        //TDirectory *scale_dir = file->mkdir("scale__1up");
        //scale_dir->cd();
        //scale.WriteUp();

        //std::cout << "INFO	- Folder scale__1down created" << std::endl;
        //file->cd();
        //scale_dir = file->mkdir("scale__1down");
        //scale_dir->cd();
        //scale.WriteDown();

        for(const auto &variation : scale_variations)
        {
            std::cout << "INFO	- Folder " << variation << " created" << std::endl;

            TDirectory *variation_dir = file->mkdir(variation);
            variation_dir->cd();

            PlotHistSet(path_to_fiducial,
                        variation,
                        truth_variable,
                        "weight/weight_var_th_MUR1_MUF1_PDF261000*" + variation);
            printer.PrintResponseVariation(variation);

            file->cd();
        }
    }
*/

    file->cd();
    file->Close();
    printer.PrintEndl();

//..................................................//
//Background inputs
    file = new TFile("background.root", "recreate");
    std::cout << "INFO	In file " << file->GetName() << ":" << std::endl;

    nominal_dir = file->mkdir(nominal_name);
    nominal_dir->cd();

    //std::vector<TString> background_inputs(vController->GetBackgroundInputs());
    auto background_channels = vController->GetBackgroundChannels();
    std::vector<double> scale_factors(vController->GetScaleFactors());

    std::vector<TH1F*> background_nominal_hists;
    for(const auto &background_inputs : background_channels)
    {
        HistCreatorTH1F background_nominal("reconstructed_" + background_inputs.at(0), path_to_nominal);
        background_nominal.SetInputs(background_inputs);
        background_nominal.ConstructHist();
        //background_nominal.Write();
        //std::cerr << "INFO	";
        background_nominal_hists.push_back(dynamic_cast<TH1F*>(background_nominal.GetHist()));
    }

    auto background_nominal_hist = background_nominal_hists.at(0);
    background_nominal_hist->Scale(scale_factors.at(0));
    for(int i = 1; i < background_nominal_hists.size(); i++)
        background_nominal_hist->Add(background_nominal_hists.at(i), scale_factors.at(i));
    background_nominal_hist->SetNameTitle("reconstructed", "reconstructed");
    background_nominal_hist->Write();
    file->cd();

    printer.PrintBackground();

    for(const auto &set : nominal_systematics)
    {
        for(const auto &systematic : set.second)
        {
            for(const auto &variation : systematic.second)
            {
                std::cout << "INFO	- Folder " << variation << " created" << std::endl;

                TDirectory *variation_dir = file->mkdir(variation);
                variation_dir->cd();

                std::vector<TH1F*> background_syst_hists;
                for(const auto &background_inputs : background_channels)
                {
                    HistCreatorTH1F background_variation("reconstructed_" + background_inputs.at(0), path_to_nominal);
                    background_variation.SetInputs(background_inputs);
                    background_variation.ConstructHist();
                    //background_variation.Write();
                    //std::cerr << "INFO	";
                    background_syst_hists.push_back(dynamic_cast<TH1F*>(background_variation.GetHist()));
                }

                auto background_syst_hist = background_syst_hists.at(0);
                background_syst_hist->Scale(scale_factors.at(0));
                for(int i = 1; i < background_syst_hists.size(); i++)
                    background_syst_hist->Add(background_syst_hists.at(i), scale_factors.at(i));
                background_syst_hist->SetNameTitle("reconstructed", "reconstructed");
                background_syst_hist->Write();

                printer.PrintGeneralVariation(variation);

                file->cd();
            }
        }
    }

    for(const auto &set : independent_systematics)
    {
        for(const auto &systematic : set.second)
        {
            for(const auto &variation : systematic.second)
            {
                std::cout << "INFO	- Folder " << variation << " created" << std::endl;

                TDirectory *variation_dir = file->mkdir(variation);
                variation_dir->cd();

                std::vector<TH1F*> background_syst_hists;
                for(const auto &background_inputs : background_channels)
                {
                    HistCreatorTH1F background_variation("reconstructed_" + background_inputs.at(0), path_to_independent + "/" + variation);
                    background_variation.SetInputs(background_inputs);
                    background_variation.ConstructHist();
                    //background_variation.Write();
                    //std::cerr << "INFO	";
                    background_syst_hists.push_back(dynamic_cast<TH1F*>(background_variation.GetHist()));
                }

                auto background_syst_hist = background_syst_hists.at(0);
                background_syst_hist->Scale(scale_factors.at(0));
                for(int i = 1; i < background_syst_hists.size(); i++)
                    background_syst_hist->Add(background_syst_hists.at(i), scale_factors.at(i));
                background_syst_hist->SetNameTitle("reconstructed", "reconstructed");
                background_syst_hist->Write();

                printer.PrintGeneralVariation(variation);

                file->cd();
            }
        }
    }

    file->cd();
    file->Close();
    printer.PrintEndl();

//..................................................//
//Data
    file = new TFile("data.root", "recreate");
    std::cout << "INFO	In file " << file->GetName() << ":" << std::endl;

    nominal_dir = file->mkdir(nominal_name);
    nominal_dir->cd();

//    std::vector<TString> data_inputs;
//    if(vController->IfFakeData())
//    {
//        data_inputs.insert(data_inputs.end(), signal_inputs.begin(),     signal_inputs.end());
//        data_inputs.insert(data_inputs.end(), background_inputs.begin(), background_inputs.end());
//    }

    if(!vController->IfFakeData())
    {
        auto data_inputs = vController->GetDataInputs();

        HistCreatorTH1F data_nominal("reconstructed", path_to_independent + "/Data");
        data_nominal.SetInputs(data_inputs);
        data_nominal.ConstructHist();
        data_nominal.Write();
    }
    else
    {
        HistCreatorTH1F data_nominal("reconstructed", path_to_nominal);
        data_nominal.SetInputs(signal_inputs);
        data_nominal.ConstructHist();
        //data_nominal.Write();
        auto data_hist = dynamic_cast<TH1F*>(data_nominal.GetHist());

        std::vector<TH1F*> data_background_hists;
        for(const auto &background_inputs : background_channels)
        {
            HistCreatorTH1F data("reconstructed_" + background_inputs.at(0), path_to_nominal);
            data.SetInputs(background_inputs);
            data.ConstructHist();
            //data.Write();
            //std::cerr << "INFO	";
            data_background_hists.push_back(dynamic_cast<TH1F*>(data.GetHist()));
        }

        auto data_background_hist = data_background_hists.at(0);
        data_background_hist->Scale(scale_factors.at(0));
        for(int i = 1; i < data_background_hists.size(); i++)
            data_background_hist->Add(data_background_hists.at(i), scale_factors.at(i));
        data_hist->Add(data_background_hist);
        data_hist->Write();
    }
   
    file->cd(); 
    file->Close();

    printer.PrintModel();
    printer.PrintRunning();
    printer.Chmod();

    std::cout << std::endl
              << "INFO	End creating outputs ..." << std::endl
              << std::endl;
}
