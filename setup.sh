#!/bin/bash

echo "setting ROOT ..."
source /cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc9-opt/setup.sh

export VBSZZ_UNFD_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export VBSZZ_UNFD_INSTALL_DIR="${VBSZZ_UNFD_DIR}/../install"
echo "setting project in ${VBSZZ_UNFD_INSTALL_DIR} ..."
if [ ! -d ${VBSZZ_UNFD_INSTALL_DIR} ]; then
    echo "project run directory doesn't exsit, creating ..."
    mkdir ${VBSZZ_UNFD_INSTALL_DIR} 
fi
export PATH="${VBSZZ_UNFD_INSTALL_DIR}/:$PATH"
